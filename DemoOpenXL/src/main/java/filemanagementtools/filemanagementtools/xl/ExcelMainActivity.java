package filemanagementtools.filemanagementtools.xl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.olivephone.sdk.view.excel.loader.ExcelLoader;
import com.olivephone.sdk.view.excel.view.TableView;

import filemanagementtools.filemanagementtools.R;


public class ExcelMainActivity extends Activity {
	ExcelLoader loader = new ExcelLoader();
	TableView tableview = null;
	private LinearLayout sheetLayout = null;
	
	private String filePath = Environment.getExternalStorageDirectory().getPath()+"/example.xlsx";
	private String license = "bHK3XLEG6Lsw+O1+7WxqNKVRmyVZKucdkJqGVxje13Raxd+No7ZxNdQ9d4I9N/pu+UF3D7XC+T6sRf0MYCtywXMF1Kgry23b8Hh740EAEv4lsW22hz0A65lYUXP9UFcMK4dCJiIt8WHS1ND180PvMs5GyvpmyrPqX1AyMpZYrTk=";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        copyFileToSdcard();
        setContentView(R.layout.activity_xl_main);
        tableview = (TableView)findViewById(R.id.tableview);
        sheetLayout = (LinearLayout)findViewById(R.id.sheetbar);
      
        loader.openExcelFile(this,tableview, filePath);
        loader.setLicense(license);
        setSheetBarLayout();
    }

	private void setSheetBarLayout() {
		sheetLayout.removeAllViews();
		List<String> sheetsName = loader.getSheetsName();
		if (sheetsName != null) {
			final int sheetsCount = sheetsName.size();
			for (int i = 0; i < sheetsCount; i++) {
				String sheetName = sheetsName.get(i);
				addSheet(i, sheetName);
			}
		}
	}

	private void addSheet(int sheetIndex, String sheetName) {
		TextView sheetBar = new TextView(this);
		sheetBar.setWidth(100);
		sheetBar.setBackgroundResource(R.drawable.excel_sheet_bar_bg);
		final int index = sheetIndex;
		sheetBar.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				loader.changeSheet(index);
			}
		});
		sheetBar.setText(sheetName);
		sheetBar.setTextColor(Color.BLACK);
		sheetBar.setTextSize(18);
		sheetBar.setGravity(Gravity.CENTER);
		sheetBar.setPadding(10, 0, 15, 0);
		sheetBar.setSingleLine();
		sheetBar.setEllipsize(TextUtils.TruncateAt.valueOf("MARQUEE"));
		sheetLayout.addView(sheetBar);
	}
	
	private void copyFileToSdcard() {
		InputStream inputstream 	= getResources().openRawResource(R.raw.example);
		byte[] buffer = new byte[1024];
		int count = 0;
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(new File(filePath));
			while ((count = inputstream.read(buffer)) > 0) {
				fos.write(buffer, 0, count);
			}
			fos.close();
		} catch (FileNotFoundException e1) {
			Toast.makeText(ExcelMainActivity.this, "Check your sdcard", Toast.LENGTH_LONG).show();
			Log.e("OliveSDKERROR", "Check your sdcard");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
