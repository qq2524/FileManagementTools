package com.tools.function.sql;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.tools.function.R;

public class SQLMainActivity extends Activity {

	private Spinner sp1;
	private Spinner sp2;
	private Button bt;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sql_main);
		
		sp1=(Spinner)findViewById(R.id.spinner1);
		sp2=(Spinner)findViewById(R.id.spinner2);
		bt= (Button) findViewById(R.id.button1);
		final BadgeView badgeView=new BadgeView(SQLMainActivity.this, bt);
		badgeView.setText("20");
		badgeView.setTextColor(Color.WHITE);
		badgeView.setBackgroundColor(Color.RED);
		badgeView.show();
		bt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				badgeView.setVisibility(View.GONE);
			}
		});
		
		
		List<String> list=new ArrayList<String>();
		list.add("广东");
		list.add("四川");
		
		ArrayAdapter<String> adapter=new ArrayAdapter<String>(SQLMainActivity.this,android.R.layout.simple_spinner_item,list);
		sp1.setAdapter(adapter);
		
		
		final Map<String,List<String>> map=new HashMap<String, List<String>>();
		List sc=new ArrayList<String>();
		sc.add("成都");
		sc.add("遂宁");
		sc.add("广安");
		map.put("四川", sc);
		List gd=new ArrayList<String>();
		gd.add("深圳");
		gd.add("越秀");
		gd.add("广州");
		map.put("广东", gd);
		sp2.setVisibility(View.GONE);
		
		sp1.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View v,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				sp2.setVisibility(View.VISIBLE);
				String s=sp1.getSelectedItem().toString();
				List<String> list=map.get(s);
				final ArrayAdapter<String> adapter2=new ArrayAdapter<String>(SQLMainActivity.this,android.R.layout.simple_spinner_item,list);
				sp2.setAdapter(adapter2);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
