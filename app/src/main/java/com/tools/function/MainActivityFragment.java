package com.tools.function;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.tools.function.appcompat.SelectFilesActivity;
import com.tools.function.cp.AudioMainActivity;
import com.tools.function.cx.CXMainActivity;
import com.tools.function.excel.ExcelMainActivity;
import com.tools.function.explorer.ExplorerMainActivity;
import com.tools.function.filemanager.MyActivity;
import com.tools.function.find.FindMainActivity;
import com.tools.function.grivider.GridMainActivity;
import com.tools.function.iv.IVMainActivity;
import com.tools.function.manager.FileManagerActivity;
import com.tools.function.ppt.PowerPointActivity;
import com.tools.function.sdcard.SDMainActivity;
import com.tools.function.sql.SQLMainActivity;
import com.tools.function.zip.ZipMainActivity;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment implements AdapterView.OnItemClickListener {

    private View view;

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_main, container, false);
        init();
        return view;
    }

    private void init() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1);
        adapter.add("IV遍历SD卡");
        adapter.add("Android 文件操作 列表显示 进入 退出 删除 复制 粘贴 新建文件等等");
        adapter.add("查找指定目录下的视频或图片");
        adapter.add("很实用的源码，包含文件管理器的基本实现思路和原理，及其方式一、首先得到手机SDCard跟目录及所有文件对象二、将这些文件目录信息包装进Adapter三、将Adapter数据设置给ListView控件显示");
        adapter.add("简单的SD卡文件管理器");
        adapter.add("文件夹的创建将raw中文件放到SD卡中将assets中文件保存到SD卡中压缩包的解压文件的删除功能");
        adapter.add("文件浏览返回路径");
        adapter.add("文件路径选择器");
        adapter.add("文件选择器");
        adapter.add("自己做的一个android数据库复制到sdcard和一个简单的角标使用");
        adapter.add("利用contentprovider扫描内存卡上所有的音频文件，扫描速度一般是300首歌需要50毫秒以内");
        adapter.add("打开ppt的demo，运行能成功，里面有个example.pptx的文件");
        adapter.add("安卓读取Excel文件获取表格数据");
        adapter.add("Android ZIP文件下载以及解压");
        ListView listView = (ListView) view.findViewById(R.id.listview);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent();
        switch (position) {
            case 0:
                intent.setClass(getActivity(), IVMainActivity.class);
                break;
            case 1:
                intent.setClass(getActivity(), FileManagerActivity.class);
                break;
            case 2:
                intent.setClass(getActivity(), FindMainActivity.class);
                break;
            case 3:
                intent.setClass(getActivity(), ExplorerMainActivity.class);
                break;
            case 4:
                intent.setClass(getActivity(), SDMainActivity.class);
                break;
            case 5:
                intent.setClass(getActivity(), CXMainActivity.class);
                break;
            case 6:
                intent.setClass(getActivity(), MyActivity.class);
                break;
            case 7:
                intent.setClass(getActivity(), SelectFilesActivity.class);
                break;
            case 8:
                intent.setClass(getActivity(), GridMainActivity.class);
                break;
            case 9:
                intent.setClass(getActivity(), SQLMainActivity.class);
                break;
            case 10:
                intent.setClass(getActivity(), AudioMainActivity.class);
                break;
            case 11:
                intent.setClass(getActivity(), PowerPointActivity.class);
                break;
            case 12:
                intent.setClass(getActivity(), ExcelMainActivity.class);
                break;
            case 13:
                intent.setClass(getActivity(), ZipMainActivity.class);
                break;
        }
        startActivity(intent);
    }
}
