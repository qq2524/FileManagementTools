package com.tools.function.find;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.tools.function.R;

public class FindMainActivity extends Activity implements OnItemClickListener {

    private String cur_path = "/sdcard/";
    private List<Picture> listPictures;
    ListView listView;

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            if (msg.what == 0) {
                List<Picture> listPictures = (List<Picture>) msg.obj;
                Toast.makeText(getApplicationContext(), "handle" + listPictures.size(), Toast.LENGTH_SHORT).show();
                MyAdapter adapter = new MyAdapter(listPictures);
                listView.setAdapter(adapter);
            }
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find);
        loadVaule();
    }

    private void loadVaule() {
        File file = new File(cur_path);
        File[] files = null;
        files = file.listFiles();
        listPictures = new ArrayList<Picture>();
        if(listPictures.isEmpty() || listPictures == null)
            return;
        for (int i = 0; i < files.length; i++) {
            Picture picture = new Picture();
            picture.setBitmap(getVideoThumbnail(files[i].getPath(), 200, 200, MediaStore.Images.Thumbnails.MICRO_KIND));
            picture.setPath(files[i].getPath());
            listPictures.add(picture);
        }
        listView = (ListView) findViewById(R.id.lv_show);
        listView.setOnItemClickListener(this);
        Message msg = new Message();
        msg.what = 0;
        msg.obj = listPictures;

        handler.sendMessage(msg);
    }


    //获取视频的缩略图
    private Bitmap getVideoThumbnail(String videoPath, int width, int height, int kind) {
        Bitmap bitmap = null;
        // 获取视频的缩略图
        bitmap = ThumbnailUtils.createVideoThumbnail(videoPath, kind);
        System.out.println("w" + bitmap.getWidth());
        System.out.println("h" + bitmap.getHeight());
        bitmap = ThumbnailUtils.extractThumbnail(bitmap, width, height, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
        return bitmap;
    }

    public class MyAdapter extends BaseAdapter {
        private List<Picture> listPictures;

        public MyAdapter(List<Picture> listPictures) {
            super();
            this.listPictures = listPictures;
        }

        @Override
        public int getCount() {
            return listPictures.size();
        }

        @Override
        public Object getItem(int position) {
            return listPictures.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View v, ViewGroup arg2) {
            View view = getLayoutInflater().inflate(R.layout.item, null);
            ImageView imageView = (ImageView) view.findViewById(R.id.iv_show);
            TextView textView = (TextView) view.findViewById(R.id.tv_show);

            imageView.setImageBitmap(listPictures.get(position).getBitmap());
            textView.setText(listPictures.get(position).getPath());
            return view;
        }
    }


    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        Toast.makeText(getApplicationContext(), "点击了" + arg2, 200).show();
        playVideo(listPictures.get(arg2).getPath());
    }

    //调用系统播放器   播放视频
    private void playVideo(String videoPath) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        String strend = "";
        if (videoPath.toLowerCase().endsWith(".mp4")) {
            strend = "mp4";
        } else if (videoPath.toLowerCase().endsWith(".3gp")) {
            strend = "3gp";
        } else if (videoPath.toLowerCase().endsWith(".mov")) {
            strend = "mov";
        } else if (videoPath.toLowerCase().endsWith(".wmv")) {
            strend = "wmv";
        }

        intent.setDataAndType(Uri.parse(videoPath), "video/" + strend);
        startActivity(intent);
    }
}
