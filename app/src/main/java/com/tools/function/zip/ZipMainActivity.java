package com.tools.function.zip;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.tools.function.R;

public class ZipMainActivity extends Activity {
	private final String TAG="ZIPMainActivity";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_zip_main);
		if (isEmulator(this)) {
			return;
		}
		  showDownLoadDialog();
		
	}

	 private void showDownLoadDialog(){
		  new AlertDialog.Builder(this).setTitle("确认")
		  .setMessage("是否下载？")
		  .setPositiveButton("是", new OnClickListener() {

		   @Override
		   public void onClick(DialogInterface dialog, int which) {
		    // TODO Auto-generated method stub
		    Log.d(TAG, "onClick 1 = "+which);
		    doDownLoadWork();
		   }
		  })
		  .setNegativeButton("否", new OnClickListener() {

		   @Override
		   public void onClick(DialogInterface dialog, int which) {
		    // TODO Auto-generated method stub
		    Log.d(TAG, "onClick 2 = "+which);
		   }
		  })
		  .show();
		 }

		 public void showUnzipDialog(){
		  new AlertDialog.Builder(this).setTitle("确认")
		  .setMessage("是否解压？")
		  .setPositiveButton("是", new OnClickListener() {

		   @Override
		   public void onClick(DialogInterface dialog, int which) {
		    // TODO Auto-generated method stub
		    Log.d(TAG, "onClick 1 = "+which);
		    doZipExtractorWork();
		   }
		  })
		  .setNegativeButton("否", new OnClickListener() {

		   @Override
		   public void onClick(DialogInterface dialog, int which) {
		    // TODO Auto-generated method stub
		    Log.d(TAG, "onClick 2 = "+which);
		   }
		  })
		  .show();
		 }

		 public void doZipExtractorWork(){
		  
		  ZipExtractorTask task = new ZipExtractorTask("/storage/emulated/legacy/appcompat_v7.zip", "/storage/emulated/legacy/", this, true);
		  task.execute();
		 }

		 private void doDownLoadWork(){
		  DownLoaderTask task = new DownLoaderTask("http://101.1.31.108:8080/appcompat_v7.zip", "/storage/emulated/legacy/", this);
		  
		  task.execute();
		 }
	//
		 public static boolean isEmulator(Context context){
             TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
             String imei = tm.getDeviceId();
             if (imei == null || imei.equals("000000000000000")){
                     return true;
             }
             return false;
     }
	
	
	
	
}
