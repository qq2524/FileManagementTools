package com.tools.function.appcompat;

import android.os.Bundle;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.tools.function.R;

/*
 @作者：郑海波 http://blog.csdn.net/nuptboyzhb
 */
public class SelectFilesActivity extends Activity {
    private Button selectBtn;
    private TextView pathView;
    private static final String DYNAMICACTION = "njupt.zhb.sendpath";
    private OnClickListener listener=null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_files);
        listener=new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(SelectFilesActivity.this,FSExplorer.class);
				startActivity(intent);
			}
		};
		selectBtn=(Button)findViewById(R.id.selectFilesBtn);
		pathView=(TextView)findViewById(R.id.filepath);
		selectBtn.setOnClickListener(listener);
		IntentFilter filter_dynamic = new IntentFilter();
		filter_dynamic.addAction(DYNAMICACTION);
		registerReceiver(dynamicReceiver, filter_dynamic);
    }
    // 2 自定义动态广播接收器，内部类,接收选择的路径
 	private BroadcastReceiver dynamicReceiver = new BroadcastReceiver() {
 		
 		@Override
 		public void onReceive(Context context, Intent intent) {
 			Log.e("GridMainActivity", "接收自定义动态注册广播消息");
 			if(intent.getAction().equals(DYNAMICACTION)){
 				String path = intent.getStringExtra("path");
 				Toast.makeText(context, path, Toast.LENGTH_SHORT).show();
 				String text="Path:"+path;
 				pathView.setText(text);
 			}
 		}
 	};
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_select_files, menu);
        return true;
    }
}
