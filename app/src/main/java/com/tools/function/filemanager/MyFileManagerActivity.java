package com.tools.function.filemanager;

import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.*;

import com.tools.function.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MyFileManagerActivity extends ListActivity {

    private List<String> items = null;   //
    private List<String> paths = null;   //
    private String rootPath = "/sdcard/";   //
    private String curPath = "/";	//
    private TextView mPath;
    public int Selectedposition=-1;
    private BaseAdapter FileAdapter;
    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.fileselect);
        mPath = (TextView) findViewById(R.id.mPath);
        Button buttonConfirm = (Button) findViewById(R.id.buttonConfirm);
        buttonConfirm.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                Intent data = new Intent(MyFileManagerActivity.this,MyActivity.class);
                data.putExtra("filePath",curPath);
                setResult(20,data);
//                startActivity(data);
                finish();

            }
        });
        Button buttonCancle = (Button) findViewById(R.id.buttonCancle);
        buttonCancle.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });
        getFileDir(rootPath);

    }

    private void getFileDir(String filePath) {
        mPath.setText(filePath);
        items = new ArrayList<String>();
        paths = new ArrayList<String>();
        File f = new File(filePath);
        File[] files = f.listFiles();

        if (!filePath.equals(rootPath)) {
            items.add("b1");
            paths.add(rootPath);
            items.add("b2");
            paths.add(f.getParent());
        }
        if (files==null){

        }else{
            for (int i = 0; i < files.length; i++) {
                File file = files[i];
                items.add(file.getName());
                paths.add(file.getPath());
            }

        }
        FileAdapter=new MyAdapter(this, items, paths,Selectedposition);
        setListAdapter(FileAdapter);

    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
//       Selectedposition=position;
        System.out.println("<<<<<<<<"+Selectedposition);
         try {
             File file = new File(paths.get(position));
             if (file.isDirectory()) {
                 curPath = paths.get(position);
                 v.setBackgroundColor(Color.WHITE);
                 getFileDir(paths.get(position));
             } else{
                 v.setSelected(true);
                 Selectedposition=position;
                 curPath = paths.get(position);
                 System.out.println("<<<<<<<<"+position);
             }

         }catch (Exception e){

             e.printStackTrace();
       }

    }


}