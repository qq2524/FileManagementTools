package com.tools.function.filemanager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.tools.function.R;

public class MyActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    private String filePath="";
    private Button choosebut;
    private EditText filename;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_main);
        filename=(EditText)findViewById(R.id.filename);
        choosebut=(Button)findViewById(R.id.choosebut);
        choosebut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MyActivity.this, MyFileManagerActivity.class);
                startActivityForResult(intent, 0);

            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(20==resultCode)
        {
            filePath=data.getExtras().getString("filePath");
            filename.setText(filePath);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
