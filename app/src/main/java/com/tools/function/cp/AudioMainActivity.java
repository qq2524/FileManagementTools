package com.tools.function.cp;

import java.util.List;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.tools.function.R;

public class AudioMainActivity extends Activity {

	private ListView listview;
	private List<MyAudio> data;
	private BaseAdapter adapter ;
	
	private RelativeLayout root ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_audio_main);
		root = (RelativeLayout) findViewById(R.id.root) ;
		data = ReadDataFromContentProvider.readAudio(this) ;
		if(null == data || data.size() == 0) {
			Toast.makeText(this, "没有扫描到音频文件!", Toast.LENGTH_LONG).show() ;
			return ;
		}
		listview = (ListView) findViewById(R.id.listview) ;
		adapter = new AudioAdapter(this, data) ;
		listview.setAdapter(adapter) ;
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
									long arg3) {
				// 获取专辑图片
				Bitmap bmp = data.get(arg2).getAlbumCover();
				if (null != bmp) {
					BitmapDrawable drawable = new BitmapDrawable(getResources(), bmp);
					root.setBackgroundDrawable(drawable);
				} else {
					root.setBackgroundColor(Color.GRAY);
				}
			}
		}) ;
		
		ReadDataFromContentProvider.readImage(this) ;
	}
}














