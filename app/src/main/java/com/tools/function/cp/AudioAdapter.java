package com.tools.function.cp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by SRain on 2015/9/30.
 */
public class AudioAdapter extends BaseAdapter {
    private Context context ;
    private List<MyAudio> data ;


    public AudioAdapter(Context context, List<MyAudio> data) {
        super();
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size() ;
    }

    @Override
    public Object getItem(int position) {
        return data.get(position) ;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view ;
        if(null != convertView) {
            view = convertView ;
        }
        else {
            view = LayoutInflater.from(context).inflate(android.R.layout.simple_list_item_2, null) ;
        }
        TextView tv1 = (TextView) view.findViewById(android.R.id.text1) ;
        tv1.setText(data.get(position).getTitle()) ;
        TextView tv2 = (TextView) view.findViewById(android.R.id.text2) ;
        tv2.setText(data.get(position).getArtist() + "\t" + data.get(position).getAlbum()) ;
        return view;
    }
}
