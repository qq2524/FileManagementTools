package com.tools.function.iv;

import java.io.File;

public class Constants {
    public static final String SEPARATOR = File.separator;
    public static final String SDCARDPATH = SEPARATOR + "storage" + SEPARATOR + "sdcard";
}
