package com.tools.function.iv;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.tools.function.R;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IVMainActivity extends Activity {
    private ListView mlv;
    private List<Map<String, Object>> mlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iv);
        mlv = (ListView) findViewById(R.id.lv);
    }

    public void click(View view) {
        refreshListView(Constants.SDCARDPATH);
    }

    //刷新ListView
    private void refreshListView(String sdcardpath) {
        mlist = getListData(sdcardpath);
        SimpleAdapter adapter = new SimpleAdapter(this, mlist, R.layout.list_item, new String[]{"icon", "name"}, new int[]{R.id.tv_title, R.id.tv_title});
        mlv.setAdapter(adapter);
        mlv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                String path = (String) mlist.get(arg2).get("path");
                if (path != null && path.length() > 0) {
                    refreshListView(path);
                }
            }
        });

    }

    //数据源
    private List<Map<String, Object>> getListData(String sdcardpath) {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            File flie = new File(sdcardpath);
            //说明不是根目录，可以返回上一级
            if (flie.getParent() != null && !"/storage".equals(flie.getParent())) {
                Map<String, Object> map1 = new HashMap<String, Object>();
                map1.put("icon", R.drawable.lastdir);
                map1.put("name", "返回上级目录");
                map1.put("path", flie.getParent());
                list.add(map1);
            }
            //返回值是file类型数组，包括文件夹和文件
            //File[] files=flie.listFiles();
            File[] files = getOrderFile(flie);
            if (files != null && files.length > 0) {

                for (File f : files) {
                    Map<String, Object> map2 = new HashMap<String, Object>();
                    //文件夹
                    if (f.isDirectory()) {
                        map2.put("icon", R.drawable.folder);
                        map2.put("name", f.getName());
                        map2.put("path", f.getAbsolutePath());
                        list.add(map2);
                    }//文件
                    else if (f.isFile()) {
                        map2.put("icon", R.drawable.pic);
                        map2.put("name", f.getName());
                        list.add(map2);
                    }
                }
            }
        } else {
            Toast.makeText(IVMainActivity.this, "SD卡无法读取", Toast.LENGTH_SHORT).show();
        }
        return list;
    }

    //得到排好序的文件夹和文件
    private File[] getOrderFile(File file) {
        File[] files = file.listFiles();
        if (files != null && files.length > 0) {
            for (int i = 0; i < files.length - 1; i++) {
                for (int j = 0; j < files.length - i - 1; j++) {
                    if (files[j].getName().compareTo(files[j + 1].getName()) > 0) {
                        File temp = files[j];
                        files[j] = files[j + 1];
                        files[j + 1] = temp;
                    }
                }
            }
        }
        return files;
    }
}
